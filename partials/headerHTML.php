<!DOCTYPE html>
    <html lang="es">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Objeto A</title>

        <!-- materialize Core CSS -->
        <link href="vendor/materialize/css/materialize.css" rel="stylesheet">
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- materialize stepper CSS -->
        <link href="vendor/materialize/css/materialize-stepper.css" rel="stylesheet">

        <!-- Custom Fonts -->
        
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="css/ionicons.min.css" rel="stylesheet" type="text/css">

        <!-- Favicon -->
        <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'>
	    <link rel='icon' href='img/favicon.ico' type='image/x-icon'>


        <!-- Rating CSS -->
        <link href="css/rating.css" rel="stylesheet">
        <!-- Theme CSS -->
        <link href="css/my-app.css" rel="stylesheet">

    </head>

    <body>