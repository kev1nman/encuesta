<?php include "partials/headerHTML.php"; include "partials/nav.php"; ?>


<div class="global">

    <div class="had-container">
        <div class="row">

            <div class="col s12">
                <div class="logo">
                    <img src="./img/logo.png" alt="">
                </div>

            </div>
            <!--
    
            <div class="col s12">
                <div class="box">
                    <div class="row">
                        <div class="col s12 center">
                            <h5 >¿ Recomendarias nuestros servicios ?</h5>
                            
                            <div class="switch">
                                <label>
                                Si
                                <input type="checkbox">
                                <span class="lever"></span>
                                No
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
    -->

            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <ul class="stepper linear">
                            <li class="step active">
                                <div class="step-title waves-effect">Pregunta 1 de 5</div>
                                <div class="step-content center">
                                        <h5 >¿ Comó evaluarias la calidad de nuestros servicios ?</h5>
                                        <div class="row">
                                            <section class='rating-widget'>  
                                                <!-- Rating Stars Box -->
                                                <div class='rating-stars text-center'>
                                                    <ul id='stars'>
                                                    <li class='star' title='Poor' data-value='1'>
                                                        <i class='fa fa-star fa-fw'></i>
                                                    </li>
                                                    <li class='star' title='Fair' data-value='2'>
                                                        <i class='fa fa-star fa-fw'></i>
                                                    </li>
                                                    <li class='star' title='Good' data-value='3'>
                                                        <i class='fa fa-star fa-fw'></i>
                                                    </li>
                                                    <li class='star' title='Excellent' data-value='4'>
                                                        <i class='fa fa-star fa-fw'></i>
                                                    </li>
                                                    <li class='star' title='WOW!!!' data-value='5'>
                                                        <i class='fa fa-star fa-fw'></i>
                                                    </li>
                                                    </ul>
                                                </div>
                                            </section>
            
                                        </div>

                                        <h5 >Por favor, cuéntanos tus observaciones</h5>
                                        
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <textarea id="textarea1" class="materialize-textarea"></textarea>
                                                <label class="rating" for="textarea1">Observaciones</label>
                                            </div>
                                        </div>


                                    <div class="step-actions">
                                        <button class="waves-effect waves-dark btn next-step">Continuar</button>
                                    </div>
                                </div>
                            </li>
                            <li class="step">
                                <div class="step-title waves-effect">Pregunta 2 de 5</div>
                                <div class="step-content center">

                                        <h5 >¿ Comó evaluarias la calidad de nuestros servicios ?</h5>
                                        <div class="row">
                                            <div class="col s12">
                                                <section class='rating-widget'>  
                                                    <!-- Rating Stars Box -->
                                                    <div class='rating-stars text-center'>
                                                        <ul id='stars'>
                                                        <li class='star' title='Poor' data-value='1'>
                                                            <i class='fa fa-star fa-fw'></i>
                                                        </li>
                                                        <li class='star' title='Fair' data-value='2'>
                                                            <i class='fa fa-star fa-fw'></i>
                                                        </li>
                                                        <li class='star' title='Good' data-value='3'>
                                                            <i class='fa fa-star fa-fw'></i>
                                                        </li>
                                                        <li class='star' title='Excellent' data-value='4'>
                                                            <i class='fa fa-star fa-fw'></i>
                                                        </li>
                                                        <li class='star' title='WOW!!!' data-value='5'>
                                                            <i class='fa fa-star fa-fw'></i>
                                                        </li>
                                                        </ul>
                                                    </div>
                                                </section>
                                            </div>
            
                                        </div>

                                        <h5 >Por favor, cuéntanos tus observaciones</h5>
                                        
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <textarea id="textarea1" class="materialize-textarea"></textarea>
                                                <label class="rating" for="textarea1">Observaciones</label>
                                            </div>
                                        </div>
                                    


                                    <div class="step-actions">
                                        <button class="waves-effect waves-dark btn next-step">Continuar</button>
                                        <button class="waves-effect waves-dark btn-flat previous-step">Atras</button>
                                    </div>
                                </div>
                            </li>
                            <li class="step">
                                    <div class="step-title waves-effect">Pregunta 3 de 5</div>
                                    <div class="step-content center">

                                            <h5 >¿ Comó evaluarias la calidad de nuestros servicios ?</h5>
                                            <div class="row">
                                                    <section class='rating-widget'>  
                                                            <!-- Rating Stars Box -->
                                                            <div class='rating-stars text-center'>
                                                                <ul id='stars'>
                                                                <li class='star' title='Poor' data-value='1'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' title='Fair' data-value='2'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' title='Good' data-value='3'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' title='Excellent' data-value='4'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' title='WOW!!!' data-value='5'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                </ul>
                                                            </div>
                                                        </section>
                
                                            </div>
    
                                            <h5 >Por favor, cuéntanos tus observaciones</h5>
                                            
                                            <div class="row">
                                                <div class="input-field col s12">
                                                    <textarea id="textarea1" class="materialize-textarea"></textarea>
                                                    <label class="rating" for="textarea1">Observaciones</label>
                                                </div>
                                            </div>
                                        
    
    
                                        <div class="step-actions">
                                            <button class="waves-effect waves-dark btn next-step">Continuar</button>
                                            <button class="waves-effect waves-dark btn-flat previous-step">Atras</button>
                                        </div>
                                    </div>
                                </li>
                                <li class="step">
                                        <div class="step-title waves-effect">Pregunta 4 de 5</div>
                                        <div class="step-content center">

                                                <h5 >¿ Comó evaluarias la calidad de nuestros servicios ?</h5>
                                                <div class="row">
                                                    <div class="col s12">
                                                            <section class='rating-widget'>  
                                                                    <!-- Rating Stars Box -->
                                                                    <div class='rating-stars text-center'>
                                                                        <ul id='stars'>
                                                                        <li class='star' title='Poor' data-value='1'>
                                                                            <i class='fa fa-star fa-fw'></i>
                                                                        </li>
                                                                        <li class='star' title='Fair' data-value='2'>
                                                                            <i class='fa fa-star fa-fw'></i>
                                                                        </li>
                                                                        <li class='star' title='Good' data-value='3'>
                                                                            <i class='fa fa-star fa-fw'></i>
                                                                        </li>
                                                                        <li class='star' title='Excellent' data-value='4'>
                                                                            <i class='fa fa-star fa-fw'></i>
                                                                        </li>
                                                                        <li class='star' title='WOW!!!' data-value='5'>
                                                                            <i class='fa fa-star fa-fw'></i>
                                                                        </li>
                                                                        </ul>
                                                                    </div>
                                                                </section>
                                                    </div>
                    
                                                </div>
        
                                                <h5 >Por favor, cuéntanos tus observaciones</h5>
                                                
                                                <div class="row">
                                                    <div class="input-field col s12">
                                                        <textarea id="textarea1" class="materialize-textarea"></textarea>
                                                        <label class="rating" for="textarea1">Observaciones</label>
                                                    </div>
                                                </div>
                                            
        
        
                                            <div class="step-actions">
                                                <button class="waves-effect waves-dark btn next-step">Continuar</button>
                                                <button class="waves-effect waves-dark btn-flat previous-step">Atras</button>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="step">
                                            <div class="step-title waves-effect">Pregunta 5 de 5</div>
                                            <div class="step-content center">

                                                    <h5 >¿ Comó evaluarias la calidad de nuestros servicios ?</h5>
                                                    <div class="row">
                                                        <div class="col s12">
                                                                <section class='rating-widget'>  
                                                                        <!-- Rating Stars Box -->
                                                                        <div class='rating-stars text-center'>
                                                                            <ul id='stars'>
                                                                            <li class='star' title='Poor' data-value='1'>
                                                                                <i class='fa fa-star fa-fw'></i>
                                                                            </li>
                                                                            <li class='star' title='Fair' data-value='2'>
                                                                                <i class='fa fa-star fa-fw'></i>
                                                                            </li>
                                                                            <li class='star' title='Good' data-value='3'>
                                                                                <i class='fa fa-star fa-fw'></i>
                                                                            </li>
                                                                            <li class='star' title='Excellent' data-value='4'>
                                                                                <i class='fa fa-star fa-fw'></i>
                                                                            </li>
                                                                            <li class='star' title='WOW!!!' data-value='5'>
                                                                                <i class='fa fa-star fa-fw'></i>
                                                                            </li>
                                                                            </ul>
                                                                        </div>
                                                                    </section>
                                                        </div>
                        
                                                    </div>
            
                                                    <h5 >Por favor, cuéntanos tus observaciones</h5>
                                                    
                                                    <div class="row">
                                                        <div class="input-field col s12">
                                                            <textarea id="textarea1" class="materialize-textarea"></textarea>
                                                            <label class="rating" for="textarea1">Observaciones</label>
                                                        </div>
                                                    </div>
                                                
            
            
                                                <div class="step-actions">
                                                    <button class="waves-effect waves-dark btn next-step">Continuar</button>
                                                    <button class="waves-effect waves-dark btn-flat previous-step">Atras</button>
                                                </div>
                                            </div>
                                        </li>
                            <li class="step">
                                <div class="step-title waves-effect">Listo!</div>
                                <div class="step-content">
                                    <h5 >Gracias por compartir tu experiencia con nosotros!</h5>
                                    
                                    <div class="step-actions">
                                        <button class="waves-effect waves-dark btn" type="submit">Enviar comentarios</button>
                                        <button class="waves-effect waves-dark btn-flat previous-step">Atras</button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>




        </div>
    </div>


    <?php include "partials/footer.php"; ?>